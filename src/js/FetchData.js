function FetchData(url) {
  return fetch(url).then(response => response.json());
}

export default FetchData;

import Educations from './Educations';

export default class Person {
  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get description() {
    return this._description;
  }

  get educations() {
    return this._educations;
  }

  constructor(name, age, description, educations) {
    this._name = name;
    this._age = age;
    this._description = description;
    educations.map(education => {
      new Educations(education.year, education.title, education.description);
    });
    this._educations = educations;
  }
}

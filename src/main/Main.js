import React from 'react';
import './main.less';
import FetchData from '../js/FetchData';
import Person from '../js/Person';
import renderNameAndAge from '../js/RenderNameAndAge';
import renderEducation from '../js/RenderEducation';

export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      educations: []
    };
  }

  componentDidMount() {
    if (!this.state.educations.length) {
      let URL = 'http://localhost:3000/person';
      FetchData(URL)
        .then(result => {
          const person = new Person(
            result.name,
            result.age,
            result.description,
            result.educations
          );
          renderNameAndAge(person);
          renderEducation(person);

          this.setState({
            educations: person.educations
          });
        })

        .catch(error => {
          document.writeln(JSON.stringify(error));
        });
    }
  }

  render() {
    return (
      <main>
        <section className="about-section">
          <h1>ABOUT ME</h1>
        </section>
        <article className="main-article">
          <h1>EDUCATION</h1>
        </article>
      </main>
    );
  }
}

import React from 'react';
import avatar from '../assets/avatar.jpg';
import './header.less';
import TitleSection from './TitleSection';

export default class Header extends React.Component {
  render() {
    return (
      <header>
        <section className="image-section">
          <img src={avatar} className="image-size  border-radius" />
        </section>
        <TitleSection />
      </header>
    );
  }
}

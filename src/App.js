import React, { Component } from 'react';
import './App.less';
import Header from './header/Header';
import Main from './main/Main';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <Main />
      </div>
    );
  }
}

export default App;
